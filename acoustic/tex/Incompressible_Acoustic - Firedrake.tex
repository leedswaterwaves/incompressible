\documentclass[a4paper,11pt]{article}

%\usepackage{marvosym}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{verbatim}
\usepackage{graphicx}
\usepackage{psfrag}
\usepackage{listings}
\usepackage{booktabs}
\usepackage{caption}


% MARGIN SETTINGS
\setlength{\voffset}{-1.1in}
\setlength{\textheight}{740pt}
%\setlength{\topmargin}{-0.5in}
\setlength{\textwidth}{6.2in}
\setlength{\oddsidemargin}{0.1in}
\setlength{\evensidemargin}{0in}
\setlength{\footskip}{20pt}

\author{Will Booker}
\title{Compressible Acoustic Waves}

\begin{document}

\maketitle

\section{Preliminaries}


A Hamiltonian formulation is a system with either an finite or infinite number of degrees of freedom with a specified structure. The system's dynamics are then described in phase-space using two geometric objects: a total energy functional,  the Hamiltonian $\mathcal{H}$, and a skew symmetric Poisson bracket $\{ , \}$.
As systems of fluid dynamics are continuous in space, we will be considering infinite dimensional dynamical systems. 



 We now consider the general state of a functional $\mathcal{F}$ for  the system in consideration, the functional's time evolution is now described by the following equation,

\begin{equation}
 \frac{ d \mathcal{F}}{dt} =\{\mathcal{F},\mathcal{H}\} .
\end{equation}
The Poisson bracket has to satisfy the following conditions :

\begin{itemize}
\item skew-symmetry: $\{\mathcal{F},\mathcal{H}\}$ = $-\{\mathcal{H},\mathcal{F}\}$,
\item linearity: $\{\alpha \mathcal{F} + \beta \mathcal{G},\mathcal{H}\}$ = $\alpha \{\mathcal{F},\mathcal{H}\}$ + $\beta\{\mathcal{G},\mathcal{H}\}$,
\item Jacobi identity: $ \{\mathcal{F},\{\mathcal{G},\mathcal{H}\}\}$ + $ \{\mathcal{G},\{\mathcal{H},\mathcal{F}\}\}$ + $ \{\mathcal{H},\{\mathcal{F},\mathcal{G}\}\}$ = 0,
\item Leibniz identity:   $\{\mathcal{F}\mathcal{G},\mathcal{H}\}$ = $\mathcal{F}\{\mathcal{G},\mathcal{H}\}$ + $\{\mathcal{F},\mathcal{H}\}\mathcal{G}$,
\end{itemize}
where $\alpha$, $\beta$ are constants, and $\mathcal{F}$, $\mathcal{G}$, $\mathcal{H}$ are arbitrary functionals. \\
We  note that the skew-symmetry condition automatically yields energy conservation for the system,
\[\frac{d \mathcal{H}}{dt} = \{\mathcal{H},\mathcal{H}\}= -\{\mathcal{H},\mathcal{H}\} = 0.\]
We first  need to define a variational derivative, 
\begin{equation}\label{eqns:var_deriv}
\delta \mathcal{H} = \lim_{\epsilon \rightarrow 0}\frac{ \mathcal{H}( u+ \epsilon\delta u)-  \mathcal{H}(u)}{\epsilon}= \bigg ( \frac{\delta  \mathcal{H}}{\delta u}, \delta u \bigg)+  \mathcal{O}(\delta u^2),
\end{equation}
where $(\cdot,\cdot)$ is the inner product for the function space $\{ u\}$.



\section{Continuum Description}

In this report we will consider a  rotating in compressible fluid in a three-dimensional domain $\Omega$ . We will assume that viscous effects and temperature effects do not influence the motion of the fluid. We assume that the resting density, $\rho_0$ and acoustic speed of sound $c^2_0$ have been scaled such that $\rho_0$ =  $c^2_0$ = 1  , and that the  equations of motions  have been linearised around a state of rest. 

We begin with an extension to the linear compressible Euler equations we have considered previously by including a rotational term, $\omega$ which is the angular velocity, 

\begin{equation}\label{eqns:euler}
\begin{aligned}
\frac{\partial \mathbf{u}  }{\partial t} &= -\nabla \rho - 2\omega \times \mathbf{u},\\
\frac{\partial \rho }{\partial t} &= - \nabla \cdot \mathbf{u} .
\end{aligned}
\end{equation}

We will take that $\partial \Omega$ i.e. the boundary of our considered domain is  taken to be solid walls, such that there is no normal flow through them. 

\[ \hat{\underline{n}}.\cdot \mathbf{u}= 0 \text{ on } \partial \Omega .\]




Hamiltonian dynamics of compressible fluid flow governed by equations \eqref{eqns:euler} is given by

\begin{equation}\label{eqns:pb} \{ \mathcal{F},  \mathcal{H}\} = \int_\Omega \frac{\delta  \mathcal{H}}{\delta \rho} \nabla \cdot \frac{\delta  \mathcal{F}}{\delta \mathbf{u}} - \frac{\delta  \mathcal{F}}{\delta \rho} \nabla \cdot \frac{\delta  \mathcal{H}}{\delta \mathbf{u}} - 2 \omega \times \frac{\delta  \mathcal{H}}{\delta \mathbf{u}} \cdot \frac{\delta  \mathcal{F}}{\delta \mathbf{u}}  \text{ d}x,\end{equation}
with its associated Hamiltonian energy functional
\[  \mathcal{H} = \int_\Omega \frac{1}{2} ( \mathbf{u}\cdot \mathbf{u} + \rho^2) \text{ d}x.\]
By using equation \eqref{eqns:var_deriv} we can show  that the variational derivatives for our associated Hamiltonian are

\begin{equation}
\frac{\delta \mathcal{H}}{\delta \rho} =\rho, \quad
\frac{\delta \mathcal{H}}{\delta \mathbf{u}}= \mathbf{u}.
\end{equation}

Dirac's theory of constrained Hamiltonian system is used to derive the linearised incompressible Euler equations from \eqref{eqns:pb}. As we are considering a linearised system, the first constraint that the density in the system is constant becomes that the density perturbations are now zero,

\[ \rho (\mathbf{x }) = 0.\]
This will act as the primary constraint we will impose onto the Poisson bracket for compressible flow. Dirac theory states that this constraint alone will not be enough to uniquely specify a system. To enforce the consistency that the primary constraint is enforced in time, a secondary constraint will be imposed unto the system. 

\[ \frac{d \mathcal{F}_\rho}{dt} = 0 = \{ \mathcal{F}_\rho , \mathcal{H} \} + \int_\Omega \lambda(\mathbf{x'} ) \{ \mathcal{F}_\rho , \rho(\mathbf{x'})\} \text{ d }\mathbf{x'} \]

The Lagrange multiplier, $\lambda$, enforces that the primary constraint remains throughout time. However from the Poisson bracket \eqref{eqns:pb} we can inspect that $\{ \mathcal{F}_\rho , \rho(\mathbf{x'}$ would be zero and therefore the Lagrange multiplier would be undetermined. By evaluating $\{ \mathcal{F}_\rho , \mathcal{H} \}$ we can however obtain a secondary constraint to enforce,

\[ 0 = \{ \mathcal{F}_\rho , \mathcal{H} \} = -  \int_\Omega \frac{\delta  \mathcal{F}}{\delta \rho} \nabla \cdot \mathbf{u} \text{ d} \mathbf{x} \]
Given that the functional is arbitrary similar to a test function, this means that the secondary constraint is that the velocity is divergence free,
\[ \nabla \cdot \mathbf{u } = 0 .\]

\section{ Discrete Description}

 We now approximate the physical domain $\Omega$ with the computational domain $\Omega_h$, which consists of $e$ non-overlapping elements. The set of all edges in the computational domain is $\Gamma$, which consists of interior edges, $\partial e $ and edges which lie on the domain boundary $\partial \Omega$. We introduce discrete variable $\lambda_h$ and $\mathbf{u}_h$, which are approximations to their continuous counterparts. 
 We start with the discrete Poisson bracket for compressible flow
 
 
 \begin{equation}\label{eqns:discretepb}
\begin{aligned}
\frac{dF}{dt} =  \{ \mathcal{F},  \mathcal{H}\} = &  \sum_e \int_e - \nabla \frac{\delta  \mathcal{H}}{\delta \rho_h}\cdot \frac{\delta  \mathcal{F}}{\delta \mathbf{u}_h} + \nabla \frac{\delta  \mathcal{F}}{\delta \rho_h}\cdot \frac{\delta  \mathcal{H}}{\delta \mathbf{u}_h} - 2\omega \times  \frac{\delta  \mathcal{H}}{\delta \mathbf{u}_h} \cdot  \frac{\delta  \mathcal{F}}{\delta \mathbf{u}_h} \text{ d}e \\
 &+ \sum_{\partial e} \int_{\partial e } \bigg(  \frac{\delta  \mathcal{H}}{\delta \rho_h^-} -\frac{\delta  \mathcal{H}}{\delta \rho_h^+}\bigg)\hat{\underline{n}} \cdot\bigg ( (1-\theta) \frac{\delta  \mathcal{F}}{\delta  \mathbf{u}_h^-}+ \theta\frac{\delta  \mathcal{F}}{\delta  \mathbf{u}_h^+} \bigg)\\
 & - \bigg(  \frac{\delta  \mathcal{F}}{\delta \rho_h^-} -\frac{\delta  \mathcal{F}}{\delta \rho_h^+}\bigg)\hat{\underline{n}} \cdot\bigg ( (1-\theta) \frac{\delta  \mathcal{H}}{\delta  \mathbf{u}_h^-}+ \theta\frac{\delta  \mathcal{H}}{\delta  \mathbf{u}_h^+} \bigg) \text{ d} \Gamma .
 \end{aligned}
 \end{equation}
We simplify the bracket by introducing the discrete divergence operator
\[ DIV(\mathbf{u}, p) = \sum_e\int_e\mathbf{u }\cdot \nabla p \text{ d}e + \sum_{\partial e } \int_{\partial e } \bigg ( p^+ - p ^-\bigg ) \underline{\hat{n}} \cdot \bigg ( (1 - \theta ) \mathbf{u}^- + \theta \mathbf{u} ^+ \bigg ) \text{ d} \Gamma .\]
The Poisson bracket simply becomes
\[ \frac{dF}{dt} =   DIV\bigg ( \frac{\delta  \mathcal{H}}{\delta \mathbf{u}_h}, \frac{\delta  \mathcal{F}}{\delta \rho_h}\bigg) -  DIV\bigg( \frac{\delta  \mathcal{F}}{\delta \mathbf{u}_h}, \frac{\delta  \mathcal{H}}{\delta \rho_h}\bigg ) - 2\omega \times  \frac{\delta  \mathcal{H}}{\delta \mathbf{u}_h} \cdot  \frac{\delta  \mathcal{F}}{\delta \mathbf{u}_h}. \]
We now apply our two constraints that the density perturbation is zero and that the velocity field remains divergence free, note that in the discrete case the condition now becomes that,
\[ DIV \bigg ( \frac{\delta  \mathcal{H}}{\delta \mathbf{u}_h} ,  \frac{\delta  \mathcal{F}}{\delta \lambda_h} \bigg ) = 0 .\]
The complete bracket for the evolution of incompressible rotating flow is now simply,
 \begin{equation}\label{eqns:incompressiblepb}
\begin{aligned}
\frac{dF}{dt} &=   -  DIV\bigg( \frac{\delta  \mathcal{F}}{\delta \mathbf{u}_h}, \frac{\delta  \mathcal{H}}{\delta \lambda_h}\bigg ) - 2\omega \times  \frac{\delta  \mathcal{H}}{\delta \mathbf{u}_h} \cdot  \frac{\delta  \mathcal{F}}{\delta \mathbf{u}_h}, \\
0 &= DIV \bigg ( \frac{\delta  \mathcal{H}}{\delta \mathbf{u}_h} ,  \frac{\delta  \mathcal{F}}{\delta \lambda_h} \bigg ).
 \end{aligned}
 \end{equation}
The Lagrange multiplier, $\lambda_h$, is solved implicitly in this form of the governing equations.
The discrete Hamiltonian functional is now 
\[ \mathcal{H} = \sum_e \int_e \frac{1}{2} \mathbf{u}_h \cdot \mathbf{u}_h.\]

\section{Divergence free initial velocity projection}

\begin{itemize}
\item divergence free proof
\item requirement that level n vel needs to be zero
\item dgfem projection of initial vel is not necessarily div free
\item project out orthogonal values
\end{itemize}

We're looking for $U^*$ such that $DIV(U^*) = 0$ and the distance $\| U^* -U \|$ is minimal. The closest vector will be a projection of $U$ onto the nullspace we are interested. 
\[ U^* =  U + U ^\perp.\]
Apply discrete divergence operator
\[ DIV(U^*) = 0 = DIV(U) + DIV (U^\perp), \]
which we rearrange for the following bilinear form for $U^\perp$
\[DIV (U^\perp) = - DIV (U).\]
This equation will be solved as part of a preprocessing process on the initial velocity. As the operator $DIV$ does not create a square matrix, we will solve this problem using a least squares solver in PETSc. 

\section{Timestepper}
\subsection{Implicit Midpoint rule}
An implicit midpoint rule is used, as it is a known property that the scheme preserves any property of the underlying ODE upto a quadratic order. This will be sufficient for our scheme to preserve its conservation of energy.

\begin{equation} \begin{aligned} \dot {y} &= f(x,y),\\
\frac{y^{n+1}- y^n}{\Delta t} &= \frac{ f(x^{n+1}, y^{n+1}) + f(x^n, y^n)}{2}, \end{aligned}\end{equation}
Applying this scheme to our Poisson bracket \eqref{eqns:incompressiblepb} yields the following discrete system, note that we take the Lagrange multiplier to be taken at time level $n + 1$. 

 \begin{equation}\label{eqns:incompressiblepb_time}
\begin{aligned}
\frac{\mathbf{u}_h^{n+1} - \mathbf{u}^n_h}{\Delta t } \cdot \frac{\delta  \mathcal{F}}{\delta \mathbf{u}_h}  &=   -  DIV\bigg( \frac{\delta  \mathcal{F}}{\delta \mathbf{u}_h}, \frac{\delta  \mathcal{H}}{\delta \lambda_h^{n+1}}\bigg ) - 2\omega \times  \frac{\delta  \mathcal{H}}{\delta \mathbf{u}_h^{n+\frac{1}{2}} }\cdot  \frac{\delta  \mathcal{F}}{\delta \mathbf{u}_h}, \\
0 &= DIV \bigg ( \frac{\delta  \mathcal{H}}{\delta \mathbf{u}_h^{n+1}} ,  \frac{\delta  \mathcal{F}}{\delta \lambda_h} \bigg ).
 \end{aligned}
 \end{equation}


\section{Firedrake form}

In Firedrake, the user specifies the problem by inputing a space-continuous weak form that governs the problem. For compressible acoustic waves, equation \eqref{eqns:discretepb} is our space continuous weak form. For linear problems, the weak form is inputted in the following form for general test function $v$ and trial function $u$,
\[ a(u,v) = L(v).\]
Here $L$ is taken to be a vector of all known information, as our problem is unsteady $L$ will include information from the previous timestep. $a$ is taken to be the vector of unknown information of unknown information. As our timestepper in consideration is an implicit midpoint rule, the two parts of the linear system can be built as follows

\[ a = F^{n+1} - \frac{\Delta t}{2} \{ F^{n+1} , H\} ,\]
\[ L = F^{n} + \frac{\Delta t}{2} \{ F^{n} , H\}. \]

The code found with this documentation will follow this same notation, we note that the forms for $a$ and $L$ will be split up into small sums. However we will group terms together as skew-symmetric counterparts, to make the code resemble the mathematical notation. 




\end{document}